/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buffer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/16 16:15:03 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 02:15:42 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

int	ft_buffer_putm(t_param *p, const char *str, unsigned int len)
{
	unsigned int	k;

	while (len > 0)
	{
		k = BUFFER_SIZE - p->buffer_len;
		if (k > len)
			k = len;
		ft_memcpy(p->buffer + p->buffer_len, str, k);
		p->buffer_len += k;
		p->tot_len += k;
		str += k;
		len -= k;
		if (len)
			ft_buffer_flush_fd(p);
	}
	return (0);
}

int	ft_buffer_putc(t_param *p, char c, long long n)
{
	long long	k;

	while (n > 0)
	{
		k = BUFFER_SIZE - p->buffer_len;
		if (k > n)
			k = n;
		ft_memset(p->buffer + p->buffer_len, c, k);
		p->buffer_len += k;
		p->tot_len += k;
		n -= k;
		if (n)
			ft_buffer_flush_fd(p);
	}
	return (0);
}

int	ft_buffer_flush_fd(t_param *p)
{
	int			n;

	n = write(p->fd, p->buffer, p->buffer_len);
	p->buffer_len = 0;
	return (n);
}
