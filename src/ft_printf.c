/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/02 14:57:36 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 02:10:04 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

int	ft_parse_fmt(t_param *p, va_list ap)
{
	t_conv			ft_conv;
	unsigned int	i;

	p->f = F_NO;
	p->mfw = 0;
	p->prcs = -1;
	p->lm = neant;
	i = p->i;
	while (ft_strchr("-0# +123456789*.lh", (p->fmt)[i]))
		i++;
	if (!ft_strchr("di%pouxXscn", (p->fmt)[i]))
		return (ft_conv_pct(p, ap));
	while (ft_strchr("-0# +123456789*.lh", (p->fmt)[p->i]))
	{
		ft_parse_flags(p);
		ft_parse_mfw(p, ap);
		ft_parse_prcs(p, ap);
		ft_parse_lm(p);
	}
	if (p->prcs >= 0)
		p->f &= ZERO_MASK;
	ft_parse_conv(p, &ft_conv);
	if (ft_conv)
		return (ft_conv(p, ap));
	return (0);
}

int	ft_printf_common(t_param *p, const char *fmt, va_list ap)
{
	int			len;

	p->fmt = fmt;
	p->i = 0;
	p->buffer_len = 0;
	p->tot_len = 0;
	while (fmt[p->i])
	{
		len = 0;
		while (fmt[p->i + len] && fmt[p->i + len] != '%')
			len++;
		ft_buffer_putm(p, fmt + p->i, len);
		p->i += len;
		if (fmt[p->i])
		{
			(p->i)++;
			if (ft_parse_fmt(p, ap) < 0)
				return (-1);
		}
	}
	ft_buffer_flush_fd(p);
	return (0);
}

int	ft_printf(const char *fmt, ...)
{
	va_list	ap;
	t_param	p;

	va_start(ap, fmt);
	p.fd = 1;
	if (ft_printf_common(&p, fmt, ap) < 0)
		return (-1);
	va_end(ap);
	return (p.tot_len);
}

int	ft_fdprintf(int fd, const char *fmt, ...)
{
	va_list	ap;
	t_param	p;

	va_start(ap, fmt);
	p.fd = fd;
	if (ft_printf_common(&p, fmt, ap) < 0)
		return (-1);
	va_end(ap);
	return (p.tot_len);
}
