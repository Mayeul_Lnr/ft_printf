/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/04 17:05:21 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 03:04:13 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

static int	ft_putubase(t_param *p, unsigned long long n, int len, char *b)
{
	char			disp[24];
	unsigned int	b_len;
	unsigned int	temp;

	b_len = 0;
	while (b[b_len])
		b_len++;
	if (!n)
		disp[0] = '0';
	temp = len;
	while (n)
	{
		len--;
		disp[len] = b[n % b_len];
		n /= b_len;
	}
	return (ft_buffer_putm(p, disp, temp));
}

static int	ft_left_uns(t_param *p, int len, int k, char *b)
{
	int	b_len;

	b_len = 0;
	while (b[b_len])
		b_len++;
	if (ft_buffer_putc(p, ' ', (p->f & F_MINUS) || p->f & F_ZERO ? 0 : k) < 0)
		return (-1);
	if (b_len == 16 && p->f & F_SHARP)
		if (ft_buffer_putm(p, b[10] == 'a' ? "0x" : "0X", 2) < 0)
			return (-2);
	if (ft_buffer_putc(p, '0', ft_max(p->prcs - len, 0)
			+ (p->f & F_ZERO && k > 0 ? k : 0)) < 0)
		return (-3);
	return (0);
}

static int	ft_fnv3(int b_len, unsigned long long n)
{
	int	ret;

	ret = 1;
	n /= b_len;
	while (n)
	{
		ret++;
		n /= b_len;
	}
	return (ret);
}

int	ft_conv_uns(t_param *p, char *b, unsigned long long n)
{
	int					k;
	int					len;
	int					b_len;

	b_len = 0;
	while (b[b_len])
		b_len++;
	len = ft_fnv3(b_len, n);
	k = p->mfw - ft_max(p->prcs, len) + (!p->prcs && !n)
		- 2 * (b_len == 16 && (p->f & F_SHARP) > 0);
	if (ft_left_uns(p, len, k, b) < 0)
		return (-1);
	if (p->prcs || n)
		if (ft_putubase(p, n, len, b) < 0)
			return (-2);
	if (p->f & F_MINUS)
		if (ft_buffer_putc(p, ' ', k) < 0)
			return (-3);
	return (0);
}

long long	ft_max(long long a, long long b)
{
	if (a > b)
		return (a);
	return (b);
}
