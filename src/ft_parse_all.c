/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_all.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/02 18:40:56 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 02:25:48 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

void	ft_parse_flags(t_param *p)
{
	char		*flag;
	static char	flagset[5] = "-0# +";
	static char	flagval[6] = {F_NO, F_MINUS, F_ZERO, F_SHARP, F_SPACE, F_PLUS};

	while ((p->fmt)[p->i])
	{
		flag = ft_strchr(flagset, (p->fmt)[p->i]);
		if (!flag)
			break ;
		p->f |= flagval[flag - flagset + 1];
		(p->i)++;
	}
	if (p->f & F_MINUS)
		p->f &= ZERO_MASK;
	if (p->f & F_PLUS)
		p->f &= SPACE_MASK;
	if (p->f & F_ZERO)
		p->f |= F_WASZERO;
}

void	ft_parse_mfw(t_param *p, va_list ap)
{
	if ((p->fmt)[p->i] == '*')
	{
		p->mfw = va_arg(ap, int);
		if (p->mfw < 0)
		{
			p->f |= F_MINUS;
			p->f &= ZERO_MASK;
			p->mfw *= -1;
		}
		(p->i)++;
	}
	else
	{
		if ((p->fmt)[p->i] >= 0 && (p->fmt)[p->i] <= '9')
			p->mfw = 0;
		while ((p->fmt)[p->i] >= '0' && (p->fmt)[p->i] <= '9')
		{
			p->mfw = p->mfw * 10 + (p->fmt)[p->i] - '0';
			(p->i)++;
		}
	}
}

void	ft_parse_prcs(t_param *p, va_list ap)
{
	if ((p->fmt)[p->i] != '.')
		return ;
	(p->i)++;
	if ((p->fmt)[p->i] == '*')
	{
		p->prcs = va_arg(ap, int);
		if (p->prcs < 0)
			p->prcs = -1;
		(p->i)++;
	}
	else
	{
		p->prcs = 0;
		while ((p->fmt)[p->i] >= '0' && (p->fmt)[p->i] <= '9')
		{
			p->prcs = p->prcs * 10 + (p->fmt)[p->i] - '0';
			(p->i)++;
		}
	}
}

void	ft_parse_lm(t_param *p)
{
	if ((p->fmt)[p->i] == 'h')
	{
		(p->i)++;
		if ((p->fmt)[p->i] == 'h')
		{
			(p->i)++;
			p->lm = hh;
		}
		else
			p->lm = h;
	}
	if ((p->fmt)[p->i] == 'l')
	{
		(p->i)++;
		if ((p->fmt)[p->i] == 'l')
		{
			(p->i)++;
			p->lm = ll;
		}
		else
			p->lm = l;
	}
}

void	ft_parse_conv(t_param *p, t_conv *ft_conv)
{
	char				*conv;
	static const char	convset[11] = "csdiuxXpon%";
	static t_conv		ft_convs[11] = {
		&ft_conv_c, &ft_conv_s, &ft_conv_d, &ft_conv_i, &ft_conv_u, &ft_conv_x,
		&ft_conv_capx, &ft_conv_p, &ft_conv_o, &ft_conv_n, &ft_conv_pct};

	if ((p->fmt)[p->i])
	{
		conv = ft_strchr(convset, (p->fmt)[p->i]);
		if (conv)
		{
			*ft_conv = ft_convs[conv - convset];
			(p->i)++;
		}
	}
	else
		*ft_conv = NULL;
}
