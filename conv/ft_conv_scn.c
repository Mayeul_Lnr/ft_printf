/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_scn.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/04 17:05:21 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/22 00:26:33 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

int	ft_conv_c(t_param *p, va_list ap)
{
	char	c;

	c = (unsigned char)va_arg(ap, int);
	if (!(p->f & F_MINUS))
		if (ft_buffer_putc(p, p->f & F_WASZERO ? '0' : ' ',
				p->mfw - 1) < 0)
			return (-1);
	if (ft_buffer_putc(p, c, 1) < 0)
		return (-3);
	if ((p->f & F_MINUS) && ft_buffer_putc(p, ' ', p->mfw - 1) < 0)
		return (-4);
	return (0);
}

int	ft_conv_s(t_param *p, va_list ap)
{
	static char	tres_nul[] = "(null)";
	char		*s;
	int			len;

	s = va_arg(ap, char *);
	if (!s)
		s = tres_nul;
	len = 0;
	while (s[len])
		len++;
	if (p->prcs >= 0 && p->prcs < len)
		len = p->prcs;
	if (p->mfw && !(p->f & F_MINUS))
		if (ft_buffer_putc(p, p->f & F_WASZERO ? '0' : ' ', p->mfw - len) < 0)
			return (-1);
	if (ft_buffer_putm(p, s, len) < 0)
		return (-3);
	if (p->mfw && (p->f & F_MINUS))
		if (ft_buffer_putc(p, ' ', p->mfw - len) < 0)
			return (-4);
	return (0);
}

int	ft_conv_n(t_param *p, va_list ap)
{
	int			*n;

	n = va_arg(ap, int *);
	*n = p->tot_len;
	return (0);
}
