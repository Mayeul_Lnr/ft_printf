/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_dipct.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/04 17:05:21 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 03:00:11 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

static int	ft_putd(t_param *p, long long n, int len)
{
	char	disp[24];
	int		temp;

	if (!n)
		disp[0] = '0';
	temp = len;
	while (n)
	{
		len--;
		disp[len] = '0' + (n % 10) * (1 - 2 * (n < 0));
		n /= 10;
	}
	return (ft_buffer_putm(p, disp, temp));
}

static int	ft_disp_signed(t_param *p, long long k, long long len, long long n)
{
	if (ft_buffer_putc(p, ' ', (n >= 0 && (p->f & F_SPACE))
			- (n >= 0 && (p->f & F_PLUS) > 0)
			+ ft_max(!((p->f & F_MINUS) || (p->f & F_ZERO)) * k, 0)) < 0)
		return (-1);
	if (n < 0 && ft_buffer_putc(p, '-', 1) < 0)
		return (-2);
	else if (n >= 0 && (p->f & F_PLUS) && ft_buffer_putc(p, '+', 1) < 0)
		return (-4);
	if (ft_buffer_putc(p, '0',
			ft_max(p->prcs - len + (n < 0), 0) + ((p->f & F_ZERO) && (k > 0))
			* (k - (n >= 0 && (p->f & F_PLUS) > 0))) < 0)
		return (-5);
	if ((p->prcs || n) && ft_putd(p, n, len - (n < 0)) < 0)
		return (-6);
	if ((p->f & F_MINUS) && ft_buffer_putc(p, ' ',
				k - (n >= 0 && (p->f & F_PLUS) > 0)) < 0)
		return (-7);
	return (0);
}

int	ft_conv_d(t_param *p, va_list ap)
{
	long long	n;
	long long	k;
	long long	len;

	if (p->lm == l)
		n = va_arg(ap, long);
	else if (p->lm == ll)
		n = va_arg(ap, long long);
	else
		n = va_arg(ap, int);
	k = n / 10;
	len = 1 + (n < 0);
	while (k)
	{
		len++;
		k /= 10;
	}
	k = p->mfw - ft_max(p->prcs + (n < 0), len)
		+ (!(p->prcs) && !n) - (n >= 0 && (p->f & F_SPACE));
	return (ft_disp_signed(p, k, len, n) < 0);
}

int	ft_conv_i(t_param *p, va_list ap)
{
	return (ft_conv_d(p, ap));
}

int	ft_conv_pct(t_param *p, va_list ap)
{
	(void)ap;
	if (!(p->f & F_MINUS))
		if (ft_buffer_putc(p, p->f & F_WASZERO ? '0' : ' ',
				p->mfw - 1) < 0)
			return (-1);
	if (ft_buffer_putc(p, '%', 1) < 0)
		return (-3);
	if ((p->f & F_MINUS) && ft_buffer_putc(p, ' ', p->mfw - 1) < 0)
		return (-4);
	return (0);
}
