/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_pouxx.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/04 17:05:21 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/18 20:27:14 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_printf.h"

int	ft_conv_p(t_param *p, va_list ap)
{
	unsigned long long	n;

	n = (unsigned long long)va_arg(ap, void *);
	p->f |= F_SHARP;
	return (ft_conv_uns(p, "0123456789abcdef", n));
}

int	ft_conv_o(t_param *p, va_list ap)
{
	unsigned long long	n;

	if (p->lm == l)
		n = va_arg(ap, unsigned long);
	else if (p->lm == ll)
		n = va_arg(ap, unsigned long long);
	else
		n = va_arg(ap, unsigned int);
	return (ft_conv_uns(p, "01234567", n));
}

int	ft_conv_u(t_param *p, va_list ap)
{
	unsigned long long	n;

	if (p->lm == l)
		n = va_arg(ap, unsigned long);
	else if (p->lm == ll)
		n = va_arg(ap, unsigned long long);
	else
		n = va_arg(ap, unsigned int);
	return (ft_conv_uns(p, "0123456789", n));
}

int	ft_conv_x(t_param *p, va_list ap)
{
	unsigned long long	n;

	if (p->lm == l)
		n = va_arg(ap, unsigned long);
	else if (p->lm == ll)
		n = va_arg(ap, unsigned long long);
	else
		n = va_arg(ap, unsigned int);
	if (!n)
		p->f &= SHARP_MASK;
	return (ft_conv_uns(p, "0123456789abcdef", n));
}

int	ft_conv_capx(t_param *p, va_list ap)
{
	unsigned long long	n;

	if (p->lm == l)
		n = va_arg(ap, unsigned long);
	else if (p->lm == ll)
		n = va_arg(ap, unsigned long long);
	else
		n = va_arg(ap, unsigned int);
	if (!n)
		p->f &= SHARP_MASK;
	return (ft_conv_uns(p, "0123456789ABCDEF", n));
}
