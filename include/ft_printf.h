/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 16:28:57 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 02:11:59 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include "./libft.h"

# define F_NO 0x00
# define F_MINUS 0x01
# define F_ZERO 0x02
# define F_SHARP 0x04
# define F_SPACE 0x08
# define F_PLUS 0x10
# define F_WASZERO 0x20

# define NO_MASK 0xff
# define MINUS_MASK 0xfe
# define ZERO_MASK 0xfd
# define SHARP_MASK 0xfb
# define SPACE_MASK 0xf7
# define PLUS_MASK 0xef
# define WASZERO_MASK 0xdf

# define BUFFER_SIZE 256

enum			e_lm{
	neant,
	l,
	ll,
	h,
	hh
};

typedef struct s_param{
	const char		*fmt;
	unsigned int	i;
	unsigned char	f;
	long long		mfw;
	long long		prcs;
	enum e_lm		lm;
	char			buffer[BUFFER_SIZE];
	unsigned int	buffer_len;
	unsigned int	tot_len;
	int				fd;
}	t_param;

typedef int	(*t_conv)(t_param *p, va_list ap);

/*
**	Structural functions :
**
**	ft_printf --> ft_parse_fmt --> ft_parse_flags
**							   --> ft_parse_mfw
**							   --> ft_parce_prcs
**							   --> ft_parse_conv
**							   --> ft_parse_lm
**							   --> ft_conv_*
*/

int				ft_printf(const char *fmt, ...);

int				ft_fdprintf(int fd, const char *fmt, ...);

int				ft_printf_common(t_param *p, const char *fmt, va_list ap);

int				ft_parse_fmt(t_param *p, va_list ap);

void			ft_parse_flags(t_param *p);

void			ft_parse_mfw(t_param *p, va_list ap);

void			ft_parse_prcs(t_param *p, va_list ap);

void			ft_parse_lm(t_param *p);

void			ft_parse_conv(t_param *p, t_conv *ft_conv);

/*
**	Useful function :
*/

long long		ft_max(long long a, long long b);

/*
**	Common conversion function shared by pouxX conversions :
*/

int				ft_conv_uns(t_param *p, char *b, unsigned long long n);

/*
**	Conversion functions.
**	Each one of those displays a specific conversion.
**	They all return the number of bytes written :
*/

int				ft_conv_c(t_param *p, va_list ap);

int				ft_conv_s(t_param *p, va_list ap);

int				ft_conv_d(t_param *p, va_list ap);

int				ft_conv_i(t_param *p, va_list ap);

int				ft_conv_u(t_param *p, va_list ap);

int				ft_conv_x(t_param *p, va_list ap);

int				ft_conv_capx(t_param *p, va_list ap);

int				ft_conv_p(t_param *p, va_list ap);

int				ft_conv_o(t_param *p, va_list ap);

int				ft_conv_n(t_param *p, va_list ap);

int				ft_conv_pct(t_param *p, va_list ap);

/*
**	Buffer management functions
*/

int				ft_buffer_putm(t_param *p, const char *str, unsigned int len);

int				ft_buffer_putc(t_param *p, char c, long long n);

int				ft_buffer_flush_fd(t_param *p);

#endif
