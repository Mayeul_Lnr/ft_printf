SRCS		=	src/ft_printf.c \
				src/ft_parse_all.c \
				src/ft_buffer.c \
				conv/ft_conv_dipct.c \
				conv/ft_conv_scn.c \
				conv/ft_conv_pouxx.c \
				conv/ft_conv_utils.c

OBJS		= ${SRCS:.c=.o}

NAME		= libftprintf.a
CC			= clang -Wall -Wextra -Werror
RM			= rm -f
AR			= ar rcs

${NAME}:	$(OBJS)
	${MAKE} -C ./libft
	cp ./libft/libft.a ./${NAME}
	${AR} ${NAME} ${OBJS}
	ranlib ${NAME}

all:		${NAME}

bonus:		${NAME}

%.o:		%.c
	${CC} -c $< -o $@

clean:
	${MAKE} clean -C ./libft
	${RM} ${OBJS} ${TEST_OBJS}

fclean:		clean
	${MAKE} fclean -C ./libft
	${RM} ${NAME}

re:			fclean all

.PHONY:		all bonus clean fclean re
